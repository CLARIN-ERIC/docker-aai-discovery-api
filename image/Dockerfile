FROM registry.gitlab.com/clarin-eric/docker-alpine-supervisor-java-tomcat-base:9.0.76-java17_1.0.1

ARG VERSION="2.3.2"
ARG DEPLOY_DIR="ROOT"

#Remove default webapp
RUN rm -rf /srv/tomcat/webapps/* \
 && mkdir -p /srv/tomcat/webapps/${DEPLOY_DIR}

#hadolint ignore=DL3003
RUN curl -kfL -o /tmp/metadata-api-${VERSION}.war \
    https://github.com/clarin-eric/discovery-service-backend/releases/download/${VERSION}/metadata-api-${VERSION}.war \
 && cd /srv/tomcat/webapps/${DEPLOY_DIR} \
 && unzip /tmp/metadata-api-${VERSION}.war \
 && rm /tmp/metadata-api-${VERSION}.war

COPY webapp/metadata-api/web.xml /srv/tomcat/webapps/${DEPLOY_DIR}/WEB-INF/web.xml
COPY webapp/config /config
COPY fluentd/conversion.conf /etc/fluentd/conf.d/conversion.conf
COPY convert_metadata.sh /usr/bin/convert_metadata.sh
COPY input.xml /srv/tomcat/webapps/input.xml
RUN chmod u+x /usr/bin/convert_metadata.sh \
 && mkdir -p /data

COPY cron_wrapper.sh /usr/bin/cron_wrapper
RUN chmod ugo+x /usr/bin/cron_wrapper

RUN echo "" >> /etc/crontabs/root \
 && echo "*/5 * * * * cron_wrapper \"Metadata conversion\" \"convert_metadata.sh --input /srv/tomcat8/webapps/input.xml --output /data/idps_clarin.json\" \"/var/log/metadata-api\" \"conversion\"" >> /etc/crontabs/root\
 && echo "*/5 * * * * cron_wrapper \"Metadata conversion\" \"convert_metadata.sh --input /srv/tomcat8/webapps/input_edugain.xml --output /data/idps_edugain.json\" \"/var/log/metadata-api\" \"conversion\"" >> /etc/crontabs/root\
 && mkdir -p /var/log/metadata-api \
 && touch /var/log/metadata-api/conversion.log

VOLUME ["/data"]
VOLUME ["/config"]