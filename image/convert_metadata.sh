#!/bin/bash

set -e

MD_API_SCHEME=${API_SCHEME:-"https"}
MD_API_HOST=${API_HOST:-"localhost"}
MD_API_PORT=${API_PORT:-"8443"}
INPUT_FILE="/srv/tomcat8/webapps/input.xml"
OUTPUT_FILE="/data/idps_clarin.json"
#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --input)
        INPUT_FILE=$2
        shift
        ;;
    --output)
        OUTPUT_FILE=$2
        shift
        ;;
    *)
        echo "Unkown option: $key"
        MODE="help"
        ;;
esac
shift # past argument or value
done

API_URL="${MD_API_SCHEME}://${MD_API_HOST}:${MD_API_PORT}/rest/metadata/discojuice"
echo "[$(date '+%Y-%m-%d %H:%M:%S')] [ INFO] Converting metadata: endpoint=${API_URL}, input file=${INPUT_FILE}"
curl -kfLsS -o "${OUTPUT_FILE}" -X POST -d @${INPUT_FILE} -H"Content-Type: application/xml" -H"Accept: application/json" "${API_URL}"

EXIT=$?
if [ ${EXIT} -eq 0 ]; then
    echo "[$(date '+%Y-%m-%d %H:%M:%S')] [ INFO] Finished succesfully"
else
    echo "[$(date '+%Y-%m-%d %H:%M:%S')] [ERROR] Failed with exit code=${EXIT}"
fi