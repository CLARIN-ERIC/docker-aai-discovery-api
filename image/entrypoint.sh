#!/bin/bash

set -e

#
# Set default values for parameters
#

MODE="run"

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -t|--tomcat)
        MODE="run"
        ;;
    -m|--metadata)
        MODE="metadata"
        ;;
    -h|--help)
        MODE="help"
        ;;
    *)
        echo "Unkown option: $key"
        MODE="help"
        ;;
esac
shift # past argument or value
done

if [ "${MODE}" == "help" ]; then
    echo ""
    echo "Usage: entrypoint.sh [-t|-m]"
    echo ""
    echo "  -t, --tomcat     Start tomcat (default)"
    echo "  -m, --metadata   Trigger metadata conversion"
    echo ""
    echo "  -h, --help       Show this help"
    echo ""
    exit 0
elif [ "${MODE}" == "run" ]; then
    start_tomcat.sh
elif [ "${MODE}" == "metadata" ]; then
    convert_metadata.sh
fi