#!/bin/bash

if [ ! $# -eq 4 ]; then
    echo "Invalid arguments"
    echo "usage: cron_wrapper.sh <name> <command> <log_dir> <log_file>"
    exit 1
fi

NAME=$1
COMMAND=$2
LOG_DIR=$3
LOG_FILE=$4

mkdir -p "${LOG_DIR}"
rm -vf "${LOG_DIR}/${LOG_FILE}_*.log"

DATE=$(date '+%Y%m%d_%H%M%S')
_LOG_FILE="${LOG_DIR}/${LOG_FILE}_${DATE}.log"
_AGGREGATED_LOG_FILE="${LOG_DIR}/${LOG_FILE}.log"

${COMMAND}  2>&1 | tee -a "${_LOG_FILE}" "${_AGGREGATED_LOG_FILE}" || (echo "${NAME} cron job failed with exit status $?"; cat "${_LOG_FILE}")